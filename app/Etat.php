<?php


namespace App;

use Illuminate\Database\Eloquent\Model;

class Etat extends Model
{
    public function offres(){
        return $this->hasMany(Offre::class);
    }

    public function applications() {
        return $this->hasMany(Application::class);
    }
}
