<?php

namespace App;

use App\Http\Resources\ContactResource;
use Illuminate\Database\Eloquent\Model;


class Contact extends Model
{
    protected $fillable = [
        'function', 'entreprise_id'
    ];
    /**
     * Relation polymorphique
     */

    public function userRole() {
        return 'contact';
    }
    public function user() {
        return $this->morphOne(User::class, 'role');
    }

    public function entreprise() {
        return $this->belongsTo(Entreprise::class);
    }

    public function offres() {
        return $this->hasMany(Offre::class);
    }

    public function detientOffre(Offre $offre) {
        return ($this->id == $offre->contact_id);
    }
    public function asResource() {
        return new ContactResource($this);
    }
}
