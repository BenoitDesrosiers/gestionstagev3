<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Resources\EtudiantResource;



class Etudiant extends Model
{
    protected $fillable = [
        'eligible', 'numero'
    ];
    /**
     * Relation polymorphique
     */

    /*
     * Efface les liens polymorphique
     *
     * User étant un lien pylymorphique, on ne peut utiliser onDelete(cascade)
     * On doit donc effacer le User associé manuellement
     */
    public function delete()
    {
        $res=parent::delete();
        if($res==true)
        {
            $u = $this->user;
            $u->delete();
        }
    }
    public function user() {
        return $this->morphOne(User::class, 'role');
    }

    public function userRole() {
        return 'etudiant';
    }

    // Relations
    public function etudiantsessions() {
        return $this->hasMany(EtudiantSession::class);
    }

    public function cvs() {
        return $this->hasMany(Cv::class);
    }

    public function lettres() {
        return $this->hasMany(Lettre::class);
    }

    public function applications() {
        return $this->hasManyThrough(Application::class, Cv::class);
    }

    // Resource
    public function asResource() {
        return new EtudiantResource($this);
    }

    // Possessions

    /**
     * Vérifi si l'étudiant est le propriétaire du CV
     * @param $id
     * @return bool  Vrai si c'est le propriétaire
     */
    public function detientCv($id) {
        // TODO changer la logique pour faire comme contact::detientOffre()
        return $this->cvs()->where('id', $id)->first() != null;
    }

    /**
     * Vérifi si l'étudiant est le propriétaire de la lettre de présentation
     * @param $id
     * @return bool  Vrai si c'est le propriétaire
     */
    public function detientLettre($id) {
        return $this->lettres()->where('id', $id)->first() != null;
    }

    /**
     * Vérifi si l'étudiant est le propriétaire de l'application
     * @param $id
     * @return bool  Vrai si c'est le propriétaire
     */
    public function detientApplication($id) {
        return $this->applications()->where('applications.id', $id)->first() != null;
    }
}
