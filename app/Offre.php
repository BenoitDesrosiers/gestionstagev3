<?php


namespace App;

use Illuminate\Database\Eloquent\Model;

class Offre extends Model
{
    protected $fillable = [
        'titre', 'description', 'etat_id', 'session_id', 'debut', 'fin', 'contact_id', 'entreposageFichier', 'nomFichier', 'programme_id'
    ];

    public function etat() {
        return $this->belongsTo(Etat::class);
    }

    public function session() {
        return $this->belongsTo(SessionScholaire::class);
    }

    public function contact() {
        return $this->belongsTo(Contact::class);
    }

    public function programme() {
        return $this->belongsTo(Programme::class);
    }

    public function applications() {
        return $this->hasMany('App\Application');
    }
}
