<?php

namespace App;

use App\Http\Resources\SuperviseurResource;
use Illuminate\Database\Eloquent\Model;


class Superviseur extends Model
{

    protected $fillable = [
        'programme_id'
    ];
    /**
     * Relation polymorphique
     */

    public function user() {
        return $this->morphOne(User::class, 'role');
    }

    public function userRole() {
        return 'superviseur';
    }

    public function programme() {
        return $this->belongsTo(Programme::class);
    }

    public function entreprise() {
        return $this->belongsToMany( Entreprise::class);
    }

    public function etudiantSessions() {
        return $this->belongsToMany(EtudiantSession::class);
    }

    public function asResource() {
        return new SuperviseurResource($this);
    }

    /**
     * Vérifi si le superviseur supervise cet étudiantSession
     * @param $id
     * @return bool  Vrai s'il supervise
     */
    public function detientEtudiantSession($id) {
        return $this->etudiantSessions()->where('id', $id)->first() != null;
    }
}
