<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Application extends Model
{
    protected $fillable = [
        'offre_id', 'lettre_id', 'cv_id', 'etat_id', 'session_id',
        'noteEtudiant', 'noteSuperviseur','noteContact'
    ];
    protected $casts = [
        'dateCreation' => 'datetime',
    ];

    public function offre() {
        return $this->belongsTo(Offre::class);
    }
    public function lettre() {
        return $this->belongsTo(Lettre::class);
    }
    public function cv() {
        return $this->belongsTo(Cv::class);
    }
    public function etat() {
        return $this->belongsTo(Etat::class);
    }

    public function session() {
        return $this->belongsTo(SessionScholaire::class);
    }
}
