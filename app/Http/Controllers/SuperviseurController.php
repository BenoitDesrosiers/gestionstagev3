<?php

namespace App\Http\Controllers;

use App\EtudiantSession;
use App\Programme;
use App\SessionScholaire;
use App\Application;
use Illuminate\Http\Request;
use App\Http\Resources\EtudiantSessionResource;
use App\Http\Resources\EtudiantSessionAvecOffresResource;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\EtudiantImport;
use App\Http\Resources\UserResource;
use App\Contact;
use App\User;

class SuperviseurController extends Controller
{
    // supervisions
    public function etudiantSessionSupervisionResource(Request $request, $sessionId)
    {
        $s= $request->user()->role;
        $es=$s->etudiantSessions()->where('session_id', $sessionId)->get();
        return EtudiantSessionResource::collection($es);
    }

    public function etudiantSessionSupervisionAvecOffresResource(Request $request, $sessionId)
    {
        $s= $request->user()->role;
        $es=$s->etudiantSessions()->where('session_id', $sessionId)->get();
        $request->request->add(['sessionId' => $sessionId]); //Hack pour envoyer la session à la resource
        return EtudiantSessionAvecOffresResource::collection($es);
    }

    public function etudiantsSessionProgrammeNonSupervisesResource(Request $request, $sessionId)
    {
        $s= $request->user()->role;
        $etudiantSessionsSupervises = $s->etudiantSessions()->where('session_id', $sessionId)->get()->pluck('id');

        $p = $s->programme;
        $etudiantProgrammeNonSupervise=$p->etudiantSessions()
            ->where('session_id', $sessionId)
            ->whereNotIn('id', $etudiantSessionsSupervises)
            ->get();
        return EtudiantSessionResource::collection($etudiantProgrammeNonSupervise);
    }

    public function etudiantsSessionProgrammeResource(Request $request, $sessionId)
    {
        $s= $request->user()->role;
        $p = $s->programme;
        $etudiantProgramme=$p->etudiantSessions()
            ->where('session_id', $sessionId)->get();
        return EtudiantSessionResource::collection($etudiantProgramme);
    }

    public function ajouterSupervision(Request $request)
    {
        $s= $request->user()->role;
        $s->etudiantSessions()->attach($request['etudiantsSessionIds']);
    }

    public function enleverSupervision(Request $request) {
        $s= $request->user()->role;
        $s->etudiantSessions()->detach($request['etudiantsSessionIds']);
    }

    public function etudiantSessionDelete(Request $request, $id)
    {
        $s= $request->user()->role;
        $es = EtudiantSession::findOrFail($id);
        // Vérifie que le superviseur supervise ce programme.
        if($s->programme == $es->programme) {
            $etudiant = $es->etudiant;
            $es->delete();
            if ($etudiant->etudiantsessions()->count() == 0) {
                // c'était le dernier programme/session auquel l'étudiant était inscrit.
                // On peut donc effacer l'étudiant.
                $etudiant->delete();
            }
        } else {
            abort(404);
        }
    }

    public function importerFichierEtudiant(Request $request)
    {
        // TODO gérer les mauvais ids un peu mieux.
        $sessionScholaire = SessionScholaire::findOrFail($request['sessionId']);
        $programme = Programme::findOrFail($request['programmeId']);
        $import = new EtudiantImport();
        $import->setSessionScholaire($sessionScholaire);
        $import->setProgramme($programme);
        Excel::import($import, $request->file('fichier') );
    }

    public function modifierNoteApplication(Request $request) //TODO valider les champs du request
    {
        $application = Application::findOrFail($request['id']);
        $application->noteSuperviseur = $request['note'];
        $application->save();
    }

    // Gestion des contacts
    public function contactsNonApprouves(Request $request)
    {
        $contacts =Contact::where('approuve', false)->get();
        $contactsUserID = $contacts->map(function ($item, $key) {
            return $item->user->id;
        });
        $users = User::whereIn('id', $contactsUserID)->get();
        return UserResource::collection( $users);
    }

    public function approuverContact(Request $request, $id)
    {
        $contact = Contact::findOrFail($id);
        $contact->approuve = true;
        $contact->save();
    }
}
