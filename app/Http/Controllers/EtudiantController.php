<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use App\Http\Requests\ChangerNoteRequest;
use App\Http\Resources\OffreResource;
use Illuminate\Http\Request;
use App\Http\Requests\AppliquerRequest;

use App\Http\Resources\CVResource;
use App\Http\Resources\LettreResource;
use App\Http\Resources\ApplicationEtOffreResource;

use App\Application;
use App\Cv;
use App\Etat;
use App\Lettre;
use App\Offre;
use App\Contact;

class EtudiantController extends Controller
{

    // CVs
    public function cvsResource(Request $request)
    {
        return CVResource::collection($request->user()->role->cvs);
    }

    // Lettres de présentation
    public function lettresResource(Request $request)
    {
        return LettreResource::collection($request->user()->role->lettres);
    }



    //Applications
    public function applicationsResource(Request $request)
    {
        return ApplicationEtOffreResource::collection($request->user()->role->applications);
    }

    public function appliquer(AppliquerRequest $request) {
        $cv = Cv::findOrFail($request['cvId'][0]);
        $lettre = Lettre::findOrFail($request['lettreId'][0]);
        $etat = Etat::where('nom', 'Ouvert')->first();
        foreach($request['offresIds'] as $offreId) {
            $offre = Offre::findOrFail($offreId);
            $application = new Application();
            $application->offre()->associate($offre);
            $application->cv()->associate($cv);
            $application->lettre()->associate($lettre);
            $application->etat()->associate($etat);
            $application->session()->associate($offre->session);
            $application->save();
        };
    }

    public function changerNoteApplication(ChangerNoteRequest $request) {
        if ($request->user()->role->detientApplication($request['applicationId'])) {
            $a = Application::findOrFail($request['applicationId']);
            $a->noteEtudiant = $request['noteEtudiant'];
            $a->save();
        } else {
            abort(404);
        }
    }

    public function applicationDelete(Request $request, $id) {
        if ($request->user()->role->detientApplication($id)) {
            Application::destroy($id);
        } else {
            abort(404);
        }
    }

    // Offres

    /**
     * Génère la liste des offres de l'étudiant présentement connecté.
     *
     * Seuls les offres:
     *   -sur lesquels l'étudiant n'a pas déjà appliquées;
     *   -associées au programme pour lequel l'étudiant est inscrit à la
     *      la session fournie paramètre;
     *   -pour les contacts qui sont approuvés
     * seront retournées
     *
     * @param Request $request
     * @param $session_id
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection|string
     */
    public function offresResource (Request $request, $session_id) {
        $e = $request->user()->role;
        $programme = $e->etudiantsessions->where('session_id', '=', $session_id)->first();
        if ($programme == null ) { return []; } // cet étudiant n'est pas inscrit à cette session
        else {
            $offres_deja_appliquees_id = $e->applications->pluck('offre_id');
            $contact_non_approuves_id = Contact::where('approuve', false)->get();
            return OffreResource::collection(Offre::where('session_id', $session_id)
                ->where('programme_id', $programme->programme_id)
                ->whereNotIn('contact_id', $contact_non_approuves_id)
                ->whereNotIn('id',$offres_deja_appliquees_id)
                ->get());
        }
    }
}
