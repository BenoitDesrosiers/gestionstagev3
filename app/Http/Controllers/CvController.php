<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

use App\Http\Resources\CVResource;

use App\Cv;


class CvController extends Controller
{
    public function indexResource(Request $request)
    {
        return CVResource::collection(CV::all());
    }

    public function create(Request $request)
    {
        $e =  $request->user()->role; // TODO vérifier que c'est un étudiant
        $path = $request->file('fichier')->store('cvs');
        $cv = new CV();
        $cv->etudiant()->associate($e);
        $cv->description = $request['description'];
        $cv->entreposage = $path;
        $cv->nomFichier = $request->file('fichier')->getClientOriginalName();
        $cv->save();
    }

    public function destroy(Request $request,$id)
    {
        if ($request->user()->role->detientCv($id)) { // TODO vérifier que c'est un étudiant, ou mettre que le superviseur peux effacer n'importe quel cv.
            $cv = CV::find($id);
            Storage::disk('local')->delete($cv->entreposage);//TODO ajouter les try catch
            CV::destroy($id);
        } else {
            abort(404);
        }
    }

    public function getPdf(Request $request, $id)
    {
        $cv = CV::findOrFail($id);
        return Storage::download($cv->entreposage, $cv->nomFichier );;
    }


}
