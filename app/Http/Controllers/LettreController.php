<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;

use Illuminate\Http\Request;

use App\Http\Resources\LettreResource;

use App\Lettre;

class LettreController extends Controller
{
    public function indexResource(Request $request)
    {
        return LettreResource::collection(Lettre::all());
    }

    public function create(Request $request)
    {
        $e =  $request->user()->role;
        $path = $request->file('fichier')->store('lettres');
        $l = new Lettre();
        $l->etudiant()->associate($e);
        $l->description = $request['description'];
        $l->entreposage = $path;
        $l->nomFichier = $request->file('fichier')->getClientOriginalName();
        $l->save();
    }

    public function destroy(Request $request,$id)
    {
        if ($request->user()->role->detientLettre($id)) {
            $l = Lettre::find($id);
            Storage::disk('local')->delete($l->entreposage);//TODO ajouter les try catch
            Lettre::destroy($id);
        } else {
            abort(404);
        }
    }


    public function getPdf(Request $request, $id)
    {
        $l = Lettre::find($id);
        return Storage::download($l->entreposage, $l->nomFichier );;

    }


}
