<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\ProgrammeResource;
use App\Programme;

class ProgrammeController extends Controller
{
    /**
     * Lister les programmes
     *
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function lister(Request $request){
        $p = Programme::all();
        return ProgrammeResource::collection($p);
    }
}
