<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Etat;
use App\Http\Resources\EtatResource;

class EtatController extends Controller
{
    /**
     * Lister les Etats
     *
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function lister(Request $request)
    {
        $e = Etat::all();
        return EtatResource::collection($e);
    }

    /**
     * Lister les Etats actifs
     *
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function listerActif(Request $request)
    {
        $e = Etat::all()->where('actif','=', true);
        return EtatResource::collection($e);
    }
}
