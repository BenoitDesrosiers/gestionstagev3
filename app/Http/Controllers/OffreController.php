<?php


namespace App\Http\Controllers;

use App\Http\Requests\OffreRequest;
use App\Http\Resources\OffreResource;
use App\Http\Resources\ApplicationEtOffreResource;
use App\Offre;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;



class OffreController extends Controller
{
    /**
     * Obtenir une offre en format json
     */
    public function getJson(int $id)
    {
        $o = Offre::findOrFail($id);
        return new OffreResource($o);
    }


    /**
     * Créer une nouvelle offre.
     *
     * @param  Illuminate\Http\Request  $request
     * @return \App\Offre
     */
    public function create(OffreRequest $request)
    {
        $path = '';
        $nomFichier = '';
        if ($request->file('fichier')) {
            $path = $request->file('fichier')->store('offres');
            $nomFichier = $request->file('fichier')->getClientOriginalName();
        }

        Offre::create([
            'titre' => $request->titre,
            'description' => $request->description,
            'etat_id' => $request->etat_id,
            'session_id' =>$request->session_id,
            'debut' => $request->debut,
            'fin' => $request->fin,
            'contact_id' =>$request->user()->role->id, //TODO a changer si le superviseur peut créer des offres
            'entreposageFichier' => $path,
            'nomFichier' => $nomFichier,
            'programme_id' =>$request->programme_id
        ]);
    }

    public function update(OffreRequest $request, $id)
    {
        $path = '';
        $nomFichier = '';
        if ($request->file('fichier')) {
            $path = $request->file('fichier')->store('offres');
            $nomFichier = $request->file('fichier')->getClientOriginalName();
        }
        $offre = Offre::find($id);
        if ($offre->nomFichier == '') {
            // il n'y avait pas de pdf d'associé
            // associe le nouveau s'il y en a un. (sinon '' )
            $offre->nomFichier = $nomFichier;
            $offre->entreposageFichier = $path;
        } else {
            // il y avait un pdf d'associé
            if ($nomFichier) {
                // et il y a un nouveau fichier d'associé
                // efface l'ancien et associe le nouveau
                Storage::disk('local')->delete($offre->entreposageFichier);//TODO ajouter les try catch
                $offre->nomFichier = $nomFichier;
                $offre->entreposageFichier = $path;
            } // sinon, tout est ok, garde l'ancien fichier associé.
        }


        $offre->titre = $request->titre;
        $offre->description = $request->description;
        $offre->etat_id = $request->etat_id;
        $offre->session_id =$request->session_id;
        $offre->debut = $request->debut;
        $offre->fin = $request->fin;
        $offre->programme_id = $request->programme_id;
        $offre->save();
    }

    public function destroy($id){
        //TODO: s'assurer que c'est le contact owner ou de l'entreprise qui efface l'offre
        $o = Offre::findOrFail($id);
        if ($o->nomFichier) {
            // il y un pdf d'associé, on doit l'effacer
            Storage::disk('local')->delete($o->entreposageFichier);//TODO ajouter les try catch
        }
        $o->delete();
    }

    public function getPdf(Request $request, $id)
    {
        $offre = Offre::findOrFail($id);
        return Storage::download($offre->entreposageFichier, $offre->nomFichier );
    }

    /**
     * Retourne les applications associées à une offre
     * @param Request $request
     * @param $id  l'id de l'offre
     */
    public function applications(Request $request, $id)
    {
        $offre = Offre::findOrFail($id);
        return ApplicationEtOffreResource::collection($offre->applications);

    }
}
