<?php

namespace App\Http\Controllers;

use App\Contact;
use Illuminate\Support\Facades\Storage;

use Illuminate\Http\Request;

use App\Http\Resources\EntrepriseResource;

use App\Entreprise;


class EntrepriseController extends Controller
{
    public function indexResource(Request $request)
    {
        //TODO le code d'accès est transféré. Ca cause un petit bris de sécurité.
        return EntrepriseResource::collection(Entreprise::all());
    }



    public function destroy(Request $request,$id)
    {
        if ($request->user()->role->detientEntreprise($id)) { // TODO vérifier que c'est un contact, ou un superviseur.
            Entreprise::destroy($id);
        } else {
            abort(404);
        }
    }

    /*
     * Mise à jour ou création d'une entreprise
     * Cette entreprise peut déjà exister, ou être une nouvelle
     * Dans le cas d'une nouvelle, l'id sera == 0
     * Elle sera associée au contact passé en paramètre.
     *
     * @param Request $request nom, telephone, description, url, codeAcces, contactId
     */
    public function majOuCreation(Request $request)
    {
        $c = Contact::findOrFail($request['contactId']);

        if ($request['id'] == 0) { // nouvelle entreprise
            $e = new Entreprise();
        } else {
            $e = Entreprise::findOrFail($request['id']);
        }
        $e->nom = $request['nom'];
        $e->telephone = $request['telephone'];
        $e->description = $request['description'];
        $e->url = $request['url'];
        $e->codeAcces = $request['codeAcces'];

        $e->save();
        $c->entreprise()->associate($e);
        $c->save();

    }
}
