<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SessionScholaireResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return[
            'id' => $this->id,
            'nom' => $this->nom,
            'courant' => $this->courant,
            'debut' => $this->debut,
            'fin' => $this->fin
        ] ;
    }
}
