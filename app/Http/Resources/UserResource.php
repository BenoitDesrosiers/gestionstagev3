<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;


class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */

    /**
     * A noter que user à des sous-classes qui sont "morph" à partir du champs 'role'
     * Lorsqu'on retourne un user, on retourne aussi l'info de la sous-classe associée.
     * Cette sous-classe doit avoir une méthode 'asResource' qui retourne une resource
     * avec les champs de cette sous-classe.
     */
    public function toArray($request)
    {
        return [
            'id'    => $this->id,
            'name'  => $this->name,
            'prenom' => $this->prenom,
            'prenom_nom' => $this->prenom.' '.$this->name,
            'email' => $this->email,
            'telephone' => $this->telephone,
            'photo_url' => $this->photo_url,
            'role' => $this->role->asResource(),
            'nouveau' => $this->nouveau

        ];
    }
}
