<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ContactResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'        => $this->id,
            'permission'      => $this->userRole(),
            'fonction'  => $this->fonction,
            'approuve' => $this->approuve,
            'associeAEntreprise' => $this->entreprise != null,
            'entreprise' => $this->entreprise != null ? $this->entreprise->asResource() : ""
        ];
    }
}
