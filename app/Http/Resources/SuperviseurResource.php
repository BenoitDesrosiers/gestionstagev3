<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SuperviseurResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'        => $this->id,
            'permission'    =>  $this->userRole(),
            'programme_id'  => $this->programme->id,
            'programme_nom' => $this->programme->nom
        ];
    }
}
