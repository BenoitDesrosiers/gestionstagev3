<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ApplicationEtOffreResource extends JsonResource
{
    /**
     * Retourne une application et l'offre associée
     * $this doit être une Application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'application'=> new ApplicationResource($this),
            'offre'=> new OffreResource($this->offre)
        ];
    }
}
