<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OffreResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'    => $this->id,
            'contact' => [
                'prenom' => $this->contact->user->prenom,
                'name'  => $this->contact->user->name,
                'entreprise' => $this->contact->entreprise->nom,
            ],
            'titre' => $this->titre,
            'description' => $this->description,
            'programme_id' => $this->programme->id,
            'programme' => $this->programme->nom.' '.$this->programme->sigle,
            'debut' => $this->debut,
            'fin' => $this->fin,
            'etat_id' => $this->etat->id,
            'etat' => $this->etat->nom,
            'session' => $this->session->nom,
            'sessionId' => $this->session->id,
            'nombreApplications' => $this->applications()->count(),
            'nomFichier' => $this->nomFichier,
        ];
    }
}
