<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ApplicationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'etat' => $this->etat->nom,
            'note' =>
                [
                    'etudiant' => $this->when($request->user()->role->userRole()=='etudiant', $this->noteEtudiant),
                    'superviseur' => $this->when($request->user()->role->userRole()=='superviseur', $this->noteSuperviseur),
                    'contact' => $this->when($request->user()->role->userRole()=='contact', $this->noteContact)
                ],
            'lettre' => new LettreResource($this->lettre),
            'cv' => new CVResource($this->cv),
            'session' => new SessionScholaireResource($this->session),
            'dateCreation' => $this->created_at,
        ];
    }
}
