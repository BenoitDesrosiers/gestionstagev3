<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CVResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'description' => $this->description,
            'nomFichier' => $this->nomFichier,
            'dateCreation' => $this->created_at,
            'estEffacable' => $this->estEffacable()
        ];
    }
}
