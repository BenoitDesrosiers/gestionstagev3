<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\User;

class EtudiantSessionAvecOffresResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        // Note: $request doit contenir 'sessionId', il faut donc l'ajouter manuellement
        //  ex: $request->request->add(['sessionId' => $sessionId]);
        return [
            'id' => $this->id,
            'etudiant' => new UserResource(User::find($this->etudiant->user->id)),
            'estEffacable' => true,
            'applications' => ApplicationEtOffreResource::collection($this->etudiant->applications()->where('session_id',$request['sessionId'])->get())
        ];
    }
}
