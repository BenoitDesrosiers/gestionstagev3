<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ChangerNoteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $e= $this->user()->role;
        return $e->detientApplication($this['applicationId']);    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'applicationId' => 'required|exists:applications,id',
            'noteEtudiant' => 'max:1000'
        ];
    }
}
