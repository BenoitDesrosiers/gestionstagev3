<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OffreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'titre' => 'required|max:255',
            'description' => 'max:10000',
            'etat_id' => 'required|exists:etats,id',
            'session_id' => 'required|exists:sessions,id',
            'debut' => 'required|date|before:fin',
            'fin' => 'required|date|after:debut',
            'programme_id' => 'required|exists:programmes,id'
        ];
    }
}
