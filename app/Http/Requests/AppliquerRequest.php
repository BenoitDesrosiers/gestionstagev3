<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AppliquerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $e= $this->user()->role;
        return $e->detientCV($this['cvId']) && $e->detientLettre($this['lettreId']);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cvId' => 'required|exists:cvs,id',
            'lettreId' => 'required|exists:lettres,id',
            'offresIds.*' => 'exists:offres,id',
            'offresIds' => 'required',
        ];
    }
}
