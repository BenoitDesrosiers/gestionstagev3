<?php

namespace App;

use App\Http\Resources\ProgrammeResource;
use Illuminate\Database\Eloquent\Model;


class Programme extends Model
{

    protected $fillable = [
        'nom', 'sigle'
    ];

    public function superviseurs() {
        return $this->hasMany(Superviseur::class);
    }

    public function etudiantSessions() {
        return $this->hasMany(EtudiantSession::class);
    }

    public function offres() {
        return $this->hasMany(Offre::class);
    }

    public function asResource() {
        return new ProgrammeResource($this); //TODO à faire
    }
}
