<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class EtudiantSession
 *      represente un étudiant pour une session en particulier.
 *
 * @package App
 */
class EtudiantSession extends Model
{
    protected $fillable = [
        'session_id', 'etudiant_id'
    ];

    public function etudiant() {
        return $this->belongsTo(Etudiant::class);
    }
    public function superviseurs() {
        return $this->belongsToMany(Superviseur::class);
    }

    public function programme() {
        return $this->belongsTo(Programme::class);
    }

    public function Session() {
        return $this->belongsTo(SessionScholaire::class);
    }
}
