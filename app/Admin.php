<?php

namespace App;

use App\Http\Resources\AdminResource;
use Illuminate\Database\Eloquent\Model;


class Admin extends Model
{


    /**
     * Relation polymorphique
     */

    public function user() {
        return $this->morphOne(User::class, 'role');
    }

    public function userRole() {
        return 'admin';
    }

    public function asResource() {
        return new AdminResource($this);
    }
}
