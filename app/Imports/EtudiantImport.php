<?php

namespace App\Imports;

use App\Etudiant;
use App\User;
use App\EtudiantSession;
use App\SessionScholaire;
use App\Programme;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Row;
use Maatwebsite\Excel\Concerns\OnEachRow;

/*
 * Importation d'un fichier xls contenant une liste d'étudiants
 */
class EtudiantImport implements OnEachRow, WithHeadingRow, WithCalculatedFormulas, WithValidation
{

    private $sessionScholaire;
    private $programme;
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function onRow(Row $row)
    {
        $ligne = $row->toArray();
        $da = $ligne['da'];
        $etudiant = Etudiant::where('numero', $da)->first();
        if($etudiant) {
            // l'étudiant existe. Il faut donc mettre à jour ses infos
            // et l'associer au programme pour cette session si nécessaire
            $u = $etudiant->user;
            $u->prenom = $ligne['prenom'];
            $u->name = $ligne['nom'];
            $u->email = $ligne['courriel'];
            $u->telephone = $ligne['telephone'];
                //'password' conserve l'ancien,
            $u->save();

            //vérifie si l'étudiant est déjà inscrit à ce programme à cette session
            $es = $etudiant->etudiantSessions()
                           ->where('session_id', $this->sessionScholaire->id )
                           ->where('programme_id', $this->programme->id )
                           ->first();
            if(!$es) { // pas inscrit
                $es = new EtudiantSession();
                $es->etudiant()->associate($etudiant);
                $es->Session()->associate($this->sessionScholaire);
                $es->programme()->associate($this->programme);
                $es->save();
            }
        } else {
            // l'étudiant n'existe pas. On crée le user et les associations.
            $u = User::create([
                'prenom' => $ligne['prenom'],
                'name' => $ligne['nom'],
                'email' => $ligne['courriel'],
                'telephone' => $ligne['telephone'],
                'password' => $ligne['mot_de_passe'],
            ]);
            $e = Etudiant::create([
                'numero' => $ligne['da'],
                'eligible' => true
            ]);
            $e->user()->save($u);

            $es = new EtudiantSession();
            $es->etudiant()->associate($e);
            $es->Session()->associate($this->sessionScholaire);
            $es->programme()->associate($this->programme);
            $es->save();
        }

    }

    public function rules(): array
    {
        return [
            'da' => 'required|max:7',
            'prenom' => 'required|max:40',
            'nom' => 'required|max:40',
            'courriel' => 'required|email|unique:App\User,email',
            'mot_de_passe' => 'required|max:255'
        ];
    }

    public function setSessionScholaire ( SessionScholaire $sessionScholaire)
    {
        $this->sessionScholaire = $sessionScholaire;
    }
    public function setProgramme ( Programme $programme)
    {
        $this->programme = $programme;
    }

    public function sessionScholaire () : SessionScholaire
    {
        return $this->sessionScholaire;
    }
    public function programme () : Programme
    {
        return $this->programme;
    }
}
