<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cv extends Model
{

    protected $table = 'cvs';
    public function etudiant() {
        return $this->belongsTo(Etudiant::class);
    }

    public function applications() {
        return $this->hasMany(Application::class);
    }

    public function estEffacable() {
        return $this->applications()->count() == 0;
    }
}
