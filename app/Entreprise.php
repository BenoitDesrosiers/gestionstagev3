<?php

namespace App;

use App\Http\Resources\EntrepriseResource;
use Illuminate\Database\Eloquent\Model;


class Entreprise extends Model
{
    protected $fillable = [
        'nom', 'telephone', 'description', 'url'
    ];


    public function contacts() {
        return $this->hasMany(Contact::class);
    }

    public function superviseurs() {
        return $this->belongsToMany( Superviseur::class);
    }

    public function offres()
    {
        return $this->hasManyThrough(Offre::class, Contact::class);
    }


    public function asResource() {
        return new EntrepriseResource($this);
    }
}
