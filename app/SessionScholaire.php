<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SessionScholaire extends Model
{
    protected $fillable = [
        'Session'
    ];

    protected $table = 'sessions';

    public function etudiant_sessions() {
        return $this->hasMany(EtudiantSession::class);
    }

    public function offres() {
        return $this->hasMany(Offre::class);
    }
}
