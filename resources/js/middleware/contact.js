import store from '~/store'

export default (to, from, next) => {
  if (store.getters['auth/permission'] !== 'contact') {
    next({ name: 'home' }) // ce n'est pas un contact, mais essaie d'aller sur une page de contact
  } else {
    next() // c'est un contact
  }
}
