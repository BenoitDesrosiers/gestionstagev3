import store from '~/store'

export default (to, from, next) => {
  if (store.getters['auth/permission'] !== 'superviseur') {
    next({ name: 'home' })
  } else {
    next()
  }
}
