import store from '~/store'

export default (to, from, next) => {
  if (store.getters['auth/permission'] !== 'etudiant') {
    next({ name: 'home' })
  } else {
    next()
  }
}
