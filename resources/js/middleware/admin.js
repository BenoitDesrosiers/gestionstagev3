import store from '~/store'

export default (to, from, next) => {
  if (store.getters['auth/permission'] !== 'admin') {
    next({ name: 'home' })
  } else {
    next()
  }
}
