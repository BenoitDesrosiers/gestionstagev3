import store from '~/store'

export default (to, from, next) => {
  if (store.getters['auth/permission'] !== 'contact') {
    next({ name: 'home' }) // ce n'est pas un contact, mais essaie d'aller sur une page de contact
  } else if (store.getters['auth/user'].role.associeAEntreprise) {
    next()  // c'est un contact associe à une entreprise
  } else {
    next({ name: 'contact.instructionsEntreprise' }) // c'est un contact, mais pas associé à une entreprise
  }
}
