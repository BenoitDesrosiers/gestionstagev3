// note: ces constantes sont définies uniquement pour que le linter soit capable
// de détecter une typo.

// auth.js
export const LOGOUT = 'LOGOUT'
export const SAVE_TOKEN = 'SAVE_TOKEN'
export const FETCH_USER = 'FETCH_USER'
export const FETCH_USER_SUCCESS = 'FETCH_USER_SUCCESS'
export const FETCH_USER_FAILURE = 'FETCH_USER_FAILURE'
export const UPDATE_USER = 'UPDATE_USER'
export const SET_NAVBAR_OPTIONS = 'SET_NAVBAR_OPTIONS'


// lang.js
export const SET_LOCALE = 'SET_LOCALE'

// sessionScholaire.js
export const SET_SESSIONS_SCHOLAIRES = 'SET_SESSIONS_SCHOLAIRES'
