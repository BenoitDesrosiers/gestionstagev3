import * as types from '../mutation-types'

// state
export const state = {
  sessionsScholaires: null
}

// getters
export const getters = {
  sessionsScholaires: state => state.sessionsScholaires,
  sessionCourante: (state) => {
    return state.sessionsScholaires.find(session => session.courant === 1)
  }

}

// mutations
export const mutations = {
  [types.SET_SESSIONS_SCHOLAIRES] (state, { options }) {
    state.sessionsScholaires = options
  }
}

// actions
export const actions = {
  setSessionsScholaires ({ commit }, options) {
    commit(types.SET_SESSIONS_SCHOLAIRES, options)
  }
}
