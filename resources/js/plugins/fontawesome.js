import Vue from 'vue'
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

// import { } from '@fortawesome/free-regular-svg-icons'

import {
  faUser, faLock, faSignOutAlt, faCog, faHome, faBuilding, faUsers,
  faAddressBook, faBold, faItalic, faUnderline, faHeading, faListUl, faListOl,
  faUndo, faRedo, faLink, faUserGraduate
} from '@fortawesome/free-solid-svg-icons'

import {
  faGithub
} from '@fortawesome/free-brands-svg-icons'

library.add(
  faUser, faLock, faSignOutAlt, faCog, faGithub, faHome, faBuilding, faUsers,
  faAddressBook, faBold, faItalic, faUnderline, faHeading, faListUl, faListOl,
  faUndo, faRedo, faLink, faUserGraduate
)

Vue.component('fa', FontAwesomeIcon)
