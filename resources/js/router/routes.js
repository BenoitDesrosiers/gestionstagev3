/* eslint-disable no-multiple-empty-lines */
function page (path) {
  return () => import(/* webpackChunkName: '' */ `~/pages/${path}`).then(m => m.default || m)
}

export default [
  { path: '/', name: 'welcome', component: page('welcome.vue') },

  { path: '/login', name: 'login', component: page('auth/login.vue') },
  { path: '/register', name: 'register', component: page('auth/register.vue') },
  { path: '/password/reset', name: 'password.request', component: page('auth/password/email.vue') },
  { path: '/password/reset/:token', name: 'password.reset', component: page('auth/password/reset.vue') },
  { path: '/email/verify/:id', name: 'verification.verify', component: page('auth/verification/verify.vue') },
  { path: '/email/resend', name: 'verification.resend', component: page('auth/verification/resend.vue') },

  { path: '/home', name: 'home', component: page('home.vue') },
  { path: '/settings',
    component: page('settings/index.vue'),
    children: [
      { path: '', redirect: { name: 'settings.profile' } },
      { path: 'profile', name: 'settings.profile', component: page('settings/profile.vue') },
      { path: 'password', name: 'settings.password', component: page('settings/password.vue') }
    ] },

  { path: '/contact',
    component: page('contacts/index.vue'),
    children: [
      { path: '', redirect: { name: 'contact.home' } },
      { path: 'home', name: 'contact.home', component: page('contacts/home.vue') },
      { path: 'instructionsEntreprise', name: 'contact.instructionsEntreprise', component: page('contacts/instructionsEntreprise.vue') },
      { path: 'entreprise', name: 'contact.entreprise', component: page('contacts/entreprise.vue') },
      { path: 'offre/create', name: 'offre.create', component: page('offres/offreC.vue') },
      { path: 'offre/:id', name: 'offre', component: page('offres/offreRU.vue'), props: true },
      { path: 'offre/:id/applications', name: 'offre.applications', component: page('offres/applications.vue'), props: true },

    ]
  },

  { path: '/etudiant',
    component: page('etudiants/index.vue'),
    children: [
      { path: '', redirect: { name: 'etudiant.home' } },
      { path: 'home', name: 'etudiant.home', component: page('etudiants/home.vue') },
    ]
  },

  { path: '/superviseur',
    component: page('superviseurs/index.vue'),
    children: [
      { path: '', redirect: { name: 'superviseur.home' } },
      { path: 'home', name: 'superviseur.home', component: page('superviseurs/home.vue') },
      { path: 'gestionSupervision', name: 'superviseur.gestionSupervision', component: page('superviseurs/gestionSupervision.vue') },
      { path: 'etudiantsProgramme', name: 'superviseur.etudiantsProgramme', component: page('superviseurs/etudiantsProgramme.vue') },
      { path: 'gestionContacts', name: 'superviseur.gestionContacts', component: page('superviseurs/gestionContacts.vue') },
    ]
  },

  { path: '/admin/home', name: 'admin.home', component: page('homeadmin.vue') },
  { path: '*', component: page('errors/404.vue') }

]
