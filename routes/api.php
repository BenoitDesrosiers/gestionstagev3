<?php

use App\Application;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Resources\EntrepriseResource;
use App\Http\Resources\OffreResource;
use App\Http\Resources\SessionScholaireResource;
use App\Http\Resources\UserResource;
use App\Http\Resources\ApplicationEtEtudiantResource;
use App\SessionScholaire;
use App\Contact;
use App\User;
use App\Offre;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'auth:api'], function () {
    Route::post('logout', 'Auth\LoginController@logout');

    Route::get('/user', function (Request $request) {
        return $request->user()->asResource();
    });

    Route::patch('settings/profile', 'Settings\ProfileController@update');
    Route::patch('settings/password', 'Settings\PasswordController@update');

    // Show de lettre et cv. N'importe qui connecté y a accès.
    Route::get('/lettre/pdf/{id}',
        ['as' => 'lettre.pdf', 'uses' => 'LettreController@getPdf']);
    Route::get('/cv/pdf/{id}',
        ['as' => 'cv.pdf', 'uses' => 'CvController@getPdf']);

    Route::get('/offre/json/{id}',
        ['as' => 'offre.json', 'uses' => 'OffreController@getJson']);
    Route::get('/offre/pdf/{id}',
        ['as' => 'offre.pdf', 'uses' => 'OffreController@getPdf']);
    // Liste des entreprises. N'importe qui connecté y a accès
    // TODO le code d'accès pour l'entreprise est transféré. Ca cause un ptit bris de sécurité
    Route::get('/entreprises',
        ['as' => 'entreprises', 'uses' => 'EntrepriseController@indexResource']);


    // Contact
    Route::group(['middleware' => 'checkUserRole:contact'], function()
    {
        //TODO créer un ctrl pour contact
        Route::get('/contact/offres', function (Request $request) {
            return OffreResource::collection($request->user()->role->offres);
        });

        Route::get('/contact/offresPourSession/{id}', function (Request $request, $id) {
            return OffreResource::collection($request->user()->role->offres->where('session_id', '=', $id));
        });
        Route::get('/entrepriseDeContact/offresPourSession/{id}', function (Request $request, $id) {
            return OffreResource::collection($request->user()->role->entreprise->offres->where('session_id', '=', $id));
        });
        Route::get( '/contact/entreprise', function (Request $request) {
            $contact = $request->user()->role;
            if ($contact->entreprise) {
                return new EntrepriseResource($request->user()->role->entreprise);
            } else {
                return null;
            }
        });

        Route::get('/contact/offre/{id}/applications', function (Request $request, $id) {
            $contact = $request->user()->role;
            $offre = Offre::findOrFail($id);
            if ($contact->detientOffre($offre)) {
                return ApplicationEtEtudiantResource::collection($offre->applications);
            } else {
                abort(403);
            }
        });

        Route::put( '/contact/modifierNoteApplication', function (Request $request) {
            // todo vérifier que l'application est sur une offre de ce contact
            $application = Application::findOrFail($request['id']);
            $application->noteContact = $request['note'];
            $application->save();
        });

        Route::get( '/contact/autresContactsMemeEntreprise', function (Request $request) {
            //liste des contacts d'une même entreprises
            $contact = $request->user()->role;
            if ($contact->approuve) {
                $autresContacts = Contact::where('entreprise_id', $contact->entreprise_id)->get(); //TODO: c'est pas trop trop objet
                $autresContactsUserId = $autresContacts->map(function ($item) {
                    return $item->user->id; //va chercher l'id du user associé à ce contact
                });
                $users = User::whereIn('id', $autresContactsUserId)->get();
                return UserResource::collection($users);
            } else {
                return null; // si le contact n'est pas approuvé, il ne peut transférer des offres.
                // Pour éliminer les hackers qui créeraient des offres bidons et qui les transféraient
            }
        });
        Route::post('/contact/transfererOffre', function (Request $request) { //todo ajouter un request
            //transfert une offre à un autre contact
            $contact = $request->user()->role;
            $offre = Offre::findOrFail($request->offreId);
            if ($contact->approuve && $offre->contact_id == $contact->id) {
                $nouveauContact = Contact::findOrFail($request->nouveauContactId);
                $offre->contact()->associate($nouveauContact);
                $offre->save();

            } else {
                abort(403); // un contact non approuvé n'aurait jamais du se rendre ici.
                // et un contact à qui n'appartient pas cette offre ne devrait pas se rendre ici non plus.
            }
        });

        Route::post('/offre/create', 'OffreController@create');
        Route::post('/offre/update/{id}', 'OffreController@update');
        Route::delete('/offre/delete/{id}', 'OffreController@destroy');
        //TODO: s'assurer que c'est le contact owner ou de l'entreprise qui efface l'offre
        Route::get('/offre/{id}/applications', 'OffreController@applications');


        Route::post('/entreprise/majOuCreation',  // pas certain que ca va dans la section contact ?
            ['as' => 'entreprise.majOuCreation', 'uses' => 'EntrepriseController@majOuCreation']);
    });

    // Etudiant
    Route::group(['middleware' => 'checkUserRole:etudiant'], function() {
        // CVs
        Route::get('/etudiant/cvs',
            ['as' => 'etudiant.cvs', 'uses' => 'EtudiantController@cvsResource']);
        Route::delete('etudiant/cv/effacer/{id}',
            ['as' => 'etudiant.cvEffacer', 'uses' => 'CvController@destroy']);
        Route::post('etudiant/cvAjouter',  // TODO changer pour cv/ajouter
            ['as' => 'etudiant.cvAjouter', 'uses' => 'CvController@create']);

        // Lettres de présentation
        Route::get('/etudiant/lettres',
            ['as' => 'etudiant.lettres', 'uses' => 'EtudiantController@lettresResource']);
        Route::delete('/etudiant/lettre/effacer/{id}',
            ['as' => 'etudiant.lettreEffacer', 'uses' => 'LettreController@destroy']);
        Route::post('etudiant/lettreAjouter',
            ['as' => 'etudiant.lettreAjouter', 'uses' => 'LettreController@create']);

        // Applications
        Route::get('/etudiant/applications', ['as' => 'etudiant.applications', 'uses' => 'EtudiantController@applicationsResource']);
        Route::post('/etudiant/appliquer',
            ['as' => 'etudiant.appliquer', 'uses' => 'EtudiantController@appliquer']);
        Route::post('/etudiant/application/note',
            ['as' => 'etudiant.changerNoteApplication', 'uses' => 'EtudiantController@changerNoteApplication']);
        Route::delete('/etudiant/application/delete/{id}',
            ['as' => 'etudiant.applicationDelete', 'uses' => 'EtudiantController@applicationDelete']);

        // Offres
        Route::get('/etudiant/offres/{session_id}',
            ['as' => 'etudiant.offres', 'uses' => 'EtudiantController@offresResource']);


    });

    // Superviseur
    Route::group(['middleware' => 'checkUserRole:superviseur'], function() {
        Route::get( '/superviseur/etudiantsSessionSupervision/{sessionId}',
            ['as' => 'superviseur.etudiantSessionSupervision', 'uses' => 'SuperviseurController@etudiantSessionSupervisionResource']);
        Route::get( '/superviseur/etudiantsSessionProgrammeNonSupervises/{session}',
            ['as' => 'superviseur.etudiantSessionProgramme', 'uses' => 'SuperviseurController@etudiantsSessionProgrammeNonSupervisesResource']);
        Route::get( '/superviseur/etudiantsSessionSupervisionAvecOffres/{sessionId}',
            ['as' => 'superviseur.etudiantSessionSupervisionAvecOffres', 'uses' => 'SuperviseurController@etudiantSessionSupervisionAvecOffresResource']);
        Route::post('superviseur/ajouterSupervision',
            ['as' => 'superviseur.ajouterSupervision', 'uses' => 'SuperviseurController@ajouterSupervision']);
        Route::post('superviseur/enleverSupervision',
            ['as' => 'superviseur.enleverSupervision', 'uses' => 'SuperviseurController@enleverSupervision']);
        Route::get( '/superviseur/programme/etudiants/{session}',
            ['as' => 'superviseur.etudiantProgramme', 'uses' => 'SuperviseurController@etudiantsSessionProgrammeResource']);
        Route::post('/superviseur/importerFichierEtudiant',
            ['as' => 'superviseur.importerFichierEtudiant', 'uses' => 'SuperviseurController@importerFichierEtudiant']);
        Route::delete('/superviseur/etudiantSession/delete/{id}',
            ['as' => 'superviseur.etudiantSessionDelete', 'uses' => 'SuperviseurController@etudiantSessionDelete']);
        Route::put('/superviseur/modifierNoteApplication',
            ['as' => 'superviseur.modifierNoteApplication', 'uses' => 'SuperviseurController@modifierNoteApplication']);

        Route::get('/superviseur/contactsNonApprouves',
            ['as' => 'superviseur.contactsNonApprouves', 'uses' => 'SuperviseurController@contactsNonApprouves']);
        Route::put('/superviseur/approuverContact/{id}',
            ['as' => 'superviseur.approuverContact', 'uses' => 'SuperviseurController@approuverContact']);

    });

});

Route::group(['middleware' => 'guest:api'], function () {
    Route::post('login', 'Auth\LoginController@login');
    Route::post('register', 'Auth\RegisterController@register');

    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset');

    Route::post('email/verify/{user}', 'Auth\VerificationController@verify')->name('verification.verify');
    Route::post('email/resend', 'Auth\VerificationController@resend');

    Route::post('oauth/{driver}', 'Auth\OAuthController@redirectToProvider');
    Route::get('oauth/{driver}/callback', 'Auth\OAuthController@handleProviderCallback')->name('oauth.callback');
});

// Les sessions sont toujours accessible, qu'on soit connecté ou pas.
Route::get('/sessions', function () {
    return  SessionScholaireResource::collection(SessionScholaire::orderBy('id','ASC')->get());
});

Route::get('/etats/lister/actifs', 'EtatController@listerActif');
Route::get('/etats/lister', 'EtatController@lister');
Route::get('/programmes/lister', 'ProgrammeController@lister');


