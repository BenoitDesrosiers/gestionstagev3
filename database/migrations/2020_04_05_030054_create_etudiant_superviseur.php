<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEtudiantSuperviseur extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('etudiant_session_superviseur', function (Blueprint $table) {
            $table->unsignedBigInteger('etudiant_session_id');
            $table->unsignedBigInteger('superviseur_id');

            $table->foreign('etudiant_session_id')->references('id')->on('etudiant_sessions')->onDelete('cascade');
            $table->foreign('superviseur_id')->references('id')->on('superviseurs')->onDelete('cascade');

            $table->primary(['etudiant_session_id', 'superviseur_id'], 'etudiant_session_superviseur_primary');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('etudiant_superviseur');
    }
}
