<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOffres extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offres', function (Blueprint $table) {
            $table->id();
            $table->string('titre', 255);
            $table->string('description', 10000)->nullable();
            $table->unsignedBigInteger('etat_id');
            $table->unsignedBigInteger('session_id');
            $table->date('debut');
            $table->date('fin');
            $table->unsignedBigInteger('contact_id');
            $table->string('entreposageFichier', 255)->nullable();
            $table->string('nomFichier', 255)->nullable();
            $table->unsignedBigInteger('programme_id');

            $table->foreign('contact_id')->references('id')->on('contacts')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('etat_id')->references('id')->on('etats')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('session_id')->references('id')->on('sessions')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('programme_id')->references('id')->on('programmes')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offres');
    }
}
