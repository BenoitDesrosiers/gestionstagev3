<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLettres extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lettres', function (Blueprint $table) {
            $table->id();
            $table->string('description', 1000)->nullable();
            $table->unsignedBigInteger('etudiant_id');
            $table->string('entreposage', 255)->nullable(); //TODO devrait pas être nullable
            $table->string('nomFichier', 255)->nullable();  // todo pas nullable
            $table->timestamps();

            $table->foreign('etudiant_id')->references('id')
                ->on('etudiants')
                ->onDelete('cascade')
                ->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lettres');
    }
}
