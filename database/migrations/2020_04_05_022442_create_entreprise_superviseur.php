<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEntrepriseSuperviseur extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entreprise_superviseur', function (Blueprint $table) {
            $table->unsignedBigInteger('entreprise_id');
            $table->unsignedBigInteger('superviseur_id');

            $table->foreign('entreprise_id')->references('id')->on('entreprises')->onDelete('cascade');
            $table->foreign('superviseur_id')->references('id')->on('superviseurs')->onDelete('cascade');

            $table->primary(['entreprise_id', 'superviseur_id']);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entreprise_superviseur');
    }
}
