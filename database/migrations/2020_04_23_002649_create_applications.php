<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApplications extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applications', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('offre_id');
            $table->unsignedBigInteger('lettre_id');
            $table->unsignedBigInteger('cv_id');
            $table->unsignedBigInteger('etat_id');
            $table->unsignedBigInteger('session_id');
            $table->string('noteEtudiant',1000)->nullable();
            $table->string('noteSuperviseur',1000)->nullable();
            $table->string('noteContact',1000)->nullable();

            $table->timestamps();

            // on ne peut effacer les dépendances sans avoir effacer l'applications.
            $table->foreign('offre_id')->references('id')->on('offres')->onDelete('restrict')->onUpdate('cascade');
            $table->foreign('lettre_id')->references('id')->on('lettres')->onDelete('restrict')->onUpdate('cascade');
            $table->foreign('cv_id')->references('id')->on('cvs')->onDelete('restrict')->onUpdate('cascade');
            $table->foreign('etat_id')->references('id')->on('etats')->onDelete('restrict')->onUpdate('cascade');
            $table->foreign('session_id')->references('id')->on('sessions')->onDelete('restrict')->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applications');
    }
}
