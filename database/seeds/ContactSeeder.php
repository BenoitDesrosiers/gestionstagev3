<?php

use App\Entreprise;
use App\User;
use App\Contact;
use Illuminate\Database\Seeder;

class ContactSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        $e = Entreprise::where('nom','=', 'Entreprise 1')->first();
        $user1 = User::all()->where('name', '=', 'contact1entp1')->first();
        $user2 = User::all()->where('name', '=', 'contact2entp1')->first();

        $x = new Contact();
        $x->fonction = 'PDG';
        $x->approuve = false;
        $x->entreprise()->associate($e);
        $x->save();
        $x->user()->save($user1);

        $x = new Contact();
        $x->fonction = 'rh';
        $x->approuve = true;
        $x->entreprise()->associate($e);
        $x->save();
        $x->user()->save($user2);

    }
}
