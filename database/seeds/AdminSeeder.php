<?php

use App\User;
use App\Admin;
use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        $user = User::all()->where('name', '=', 'admin')->first();

        $admin = new Admin();
        $admin->save();

        $admin->user()->save($user);
    }
}
