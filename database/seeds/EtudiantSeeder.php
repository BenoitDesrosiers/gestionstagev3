<?php

use App\User;
use App\Etudiant;
use Illuminate\Database\Seeder;

class EtudiantSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        $users = User::all()->where('name', '=', 'etudiant');
        $i = 0;
        foreach ($users as $u) {
            $etudiant = new Etudiant();
            $etudiant->eligible = true;
            $etudiant->numero = "123456".$i++; // changé si on a + que 10 étudiants.
            $etudiant->save();

            $etudiant->user()->save($u);
        }
    }
}
