<?php

use Illuminate\Database\Seeder;

use App\User;
use App\Lettre;

class LettreSeeder extends Seeder
{
    public function run()
    {
        $e1 = User::where('name', '=', 'etudiant')->first()->role;

        $l = new Lettre();
        $l->description = 'lettre #1';
        $l->etudiant()->associate($e1);
        $l->save();

        $l = new Lettre();
        $l->description = 'lettre #2';
        $l->etudiant()->associate($e1);
        $l->save();

    }
}
