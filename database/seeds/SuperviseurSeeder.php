<?php

use App\Programme;
use App\User;
use App\Superviseur;
use Illuminate\Database\Seeder;

class SuperviseurSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        $p = Programme::all();
        $users = User::all()->where('name', '=', 'superviseur');

        foreach($users as $u) {
            $x = new Superviseur();
            $x->programme()->associate($p->random());
            $x->save();

            $x->user()->save($u);
        }


    }
}
