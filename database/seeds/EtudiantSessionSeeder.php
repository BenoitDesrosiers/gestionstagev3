<?php

use Illuminate\Database\Seeder;
use App\EtudiantSession;
use App\Etudiant;
use App\SessionScholaire;
use App\Programme;

class EtudiantSessionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //


        $ss = SessionScholaire::all();
        $etudiants = Etudiant::all();
        $p = Programme::all()->first();
        foreach ($etudiants as $e) {
            foreach($ss as $s) {
                $es = new EtudiantSession();
                $es->etudiant()->associate($e);
                $es->Session()->associate($s);
                $es->programme()->associate($p);
                $es->save();
            }
        }


    }
}
