<?php

use App\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        $admin = new User();
        $admin->name = 'admin';
        $admin->prenom = 'admin';
        $admin->email = 'admin@example.com';
        $admin->password = bcrypt('password');
        $admin->save();


        $superviseur = new User();
        $superviseur->name = 'superviseur';
        $superviseur->prenom = 'superviseur';
        $superviseur->email = 'superviseur@example.com';
        $superviseur->password = bcrypt('password');
        $superviseur->save();

        $etudiant = new User();
        $etudiant->name = 'etudiant';
        $etudiant->prenom = 'etudiant';
        $etudiant->email = 'etudiant@example.com';
        $etudiant->password = bcrypt('password');
        $etudiant->save();

        $etudiant = new User();
        $etudiant->name = 'etudiant';
        $etudiant->prenom = 'etudiant2';
        $etudiant->email = 'etudiant2@example.com';
        $etudiant->password = bcrypt('password');
        $etudiant->save();

        $etudiant = new User();
        $etudiant->name = 'etudiant';
        $etudiant->prenom = 'etudiant3';
        $etudiant->email = 'etudiant3@example.com';
        $etudiant->password = bcrypt('password');
        $etudiant->save();

        $contact = new User();
        $contact->name = 'contact1entp1';
        $contact->prenom = 'contactp';
        $contact->email = 'contact1@example.com';
        $contact->password = bcrypt('password');
        $contact->save();

        $contact = new User();
        $contact->name = 'contact2entp1';
        $contact->prenom = 'contactp';
        $contact->email = 'contact2@example.com';
        $contact->password = bcrypt('password');
        $contact->save();


    }
}
