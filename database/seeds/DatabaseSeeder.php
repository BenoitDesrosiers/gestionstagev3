<?php

use App\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('applications')->delete();
        DB::table('lettres')->delete();
        DB::table('cvs')->delete();
        DB::table('etudiant_session_superviseur')->delete();
        DB::table('etudiant_sessions')->delete();
        DB::table('entreprise_superviseur')->delete();
        DB::table('superviseurs')->delete();
        DB::table('programmes')->delete();
        DB::table('contacts')->delete();
        DB::table('entreprises')->delete();
        DB::table('etudiants')->delete();
        DB::table('admins')->delete();
        DB::table('users')->delete();
        DB::table('Sessions')->delete();
        DB::table('etats')->delete();
        DB::table('offres')->delete();


        $this->call(SessionSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(AdminSeeder::class);
        $this->call(EtudiantSeeder::class);

        $this->call(EntrepriseSeeder::class);
        $this->call(ContactSeeder::class);
        $this->call(ProgrammeSeeder::class);
        $this->call(SuperviseurSeeder::class);

        $this->call(EtatSeeder::class);
        $this->call(OffreSeeder::class);

        $this->call(EntrepriseSuperviseurSeeder::class);
        $this->call(EtudiantSessionSeeder::class);
        $this->call(SuperviseurEtudiantSessionSeeder::class);
        $this->call(CVSeeder::class);
        $this->call(LettreSeeder::class);
        $this->call(ApplicationSeeder::class);

    }
}
