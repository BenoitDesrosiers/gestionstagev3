<?php

use Illuminate\Database\Seeder;
use App\Etat;

class EtatSeeder extends Seeder
{
    public function run()
    {
        $e = new Etat();
        $e->categorie = 'Tout';
        $e->nom = 'Brouillon';
        $e->actif = true;
        $e->save();

        $e = new Etat();
        $e->categorie = 'Tout';
        $e->nom = 'Ouvert';
        $e->actif = true;
        $e->save();

        $e = new Etat();
        $e->categorie = 'Tout';
        $e->nom = 'À contacter';
        $e->actif = true;
        $e->save();

        $e = new Etat();
        $e->categorie = 'Tout';
        $e->nom = 'En attente';
        $e->actif = true;
        $e->save();

        $e = new Etat();
        $e->categorie = 'Tout';
        $e->nom = 'Fermé';
        $e->actif = false;
        $e->save();

        $e = new Etat();
        $e->categorie = 'Tout';
        $e->nom = 'Annulé';
        $e->actif = false;
        $e->save();

        $e = new Etat();
        $e->categorie = 'Tout';
        $e->nom = 'Accepté';
        $e->actif = false;
        $e->save();

        $e = new Etat();
        $e->categorie = 'Tout';
        $e->nom = 'Refusé';
        $e->actif = true;
        $e->save();
    }
}
