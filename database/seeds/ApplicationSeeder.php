<?php
use App\Programme;
use App\Application;
use App\Etat;
use App\User;
use App\SessionScholaire;
use App\Offre;
use Illuminate\Database\Seeder;

class ApplicationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $os = Offre::all();
        $sc = SessionScholaire::where('courant','=',true)->first();
        $e = Etat::all();
        $e1 = User::where('name', '=', 'etudiant')->first()->role;

        //foreach ($os as $offre) {
        $offre = $os->first();
            $a = new Application();
            $a->etat()->associate($e->random());
            $a->offre()->associate($offre);
            $a->cv()->associate($e1->cvs->first());
            $a->lettre()->associate($e1->lettres->first());
            $a->session()->associate($sc);
            $a->noteEtudiant = 'note étudiant';
            $a->noteSuperviseur = 'note superviseur';
            $a->noteContact = 'note contact';

            $a->save();
        //}
    }
}
