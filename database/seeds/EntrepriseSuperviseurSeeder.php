<?php

use Illuminate\Database\Seeder;
use App\Entreprise;
use App\Superviseur;

class EntrepriseSuperviseurSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $es = Entreprise::all();
        $ss = Superviseur::all();

        foreach ($es as $e) {
            $e->superviseurs()->attach($ss->random());
        }

    }
}
