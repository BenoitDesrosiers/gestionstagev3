<?php

use App\Entreprise;
use Illuminate\Database\Seeder;

class EntrepriseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {


        $entreprise = new Entreprise();
        $entreprise->nom = 'Entreprise 1';
        $entreprise->telephone = '111-111-1111';
        $entreprise->description = 'desc entreprise 1';
        $entreprise->codeAcces = '123';
        $entreprise->url = 'entreprise1.com';
        $entreprise->save();

        $entreprise = new Entreprise();
        $entreprise->nom = 'Entreprise 2';
        $entreprise->telephone = '222-222-2222';
        $entreprise->description = 'desc entreprise 2';
        $entreprise->url = 'entreprise2.com';
        $entreprise->codeAcces = '123';
        $entreprise->save();

    }
}
