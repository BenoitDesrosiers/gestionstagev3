<?php
use App\Programme;
use App\Etat;
use App\User;
use App\SessionScholaire;
use App\Offre;
use Illuminate\Database\Seeder;

class OffreSeeder extends Seeder
{
    public function run()
    {
        $e = Etat::all();
        $s = SessionScholaire::all();
        $sc = SessionScholaire::where('courant','=',true)->first();
        $c1 = User::where('name', '=', 'contact1entp1')->first()->role;
        $c2 = User::where('name', '=', 'contact2entp1')->first()->role;
        $p = Programme::all();

        $o = new Offre();
        $o->titre = 'Tech';
        $o->description = 'Tech reseau';
        $o->etat()->associate($e->random());
        $o->session()->associate($sc);
        $o->debut = '2020/03/02';
        $o->fin = '2020/05/02';
        $o->contact()->associate($c1);
        $o->programme()->associate($p->random());
        $o->save();

        $o = new Offre();
        $o->titre = 'offres 2';
        $o->description = 'desc offres 2';
        $o->etat()->associate($e->random());
        $o->session()->associate($s->random());
        $o->debut = '2020/03/02';
        $o->fin = '2020/05/02';
        $o->contact()->associate($c1);
        $o->programme()->associate($p->random());
        $o->save();

        $o = new Offre();
        $o->titre = 'Programmeur';
        $o->description = 'Pro programmeur';
        $o->etat()->associate($e->random());
        $o->session()->associate($sc);
        $o->debut = '2020/03/15';
        $o->fin = '2020/05/15';
        $o->contact()->associate($c1);
        $o->programme()->associate($p->random());
        $o->save();

        $o = new Offre();
        $o->titre = 'Programmeur 2';
        $o->description = 'Pro programmeur 2';
        $o->etat()->associate($e->random());
        $o->session()->associate($sc);
        $o->debut = '2020/03/15';
        $o->fin = '2020/05/15';
        $o->contact()->associate($c2);
        $o->programme()->associate($p->random());
        $o->save();
    }
}
