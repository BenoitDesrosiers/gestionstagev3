<?php

use Illuminate\Database\Seeder;
use App\SessionScholaire;

class SessionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $s = new SessionScholaire();
        $s->nom = 'H21';
        $s->courant = true;
        $s->debut = "2021/03/15";
        $s->fin = "2021/05/21";
        $s->save();




    }
}
