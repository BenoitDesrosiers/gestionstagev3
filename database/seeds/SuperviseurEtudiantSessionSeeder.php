<?php

use Illuminate\Database\Seeder;
use App\Superviseur;
use App\EtudiantSession;


class SuperviseurEtudiantSessionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ss = Superviseur::all();

        foreach($ss as $s) {
            $p = $s->programme;
            $ess =  $p->etudiantsessions;

            foreach($ess as $es ) {
                $s->etudiantsessions()->attach($es);
            }

        }
    }
}
