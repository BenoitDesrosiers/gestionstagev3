<?php

use App\Programme;

use Illuminate\Database\Seeder;

class ProgrammeSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $x = new Programme();
        $x->nom = 'Informatique';
        $x->sigle = '420-B0';
        $x->save();

    }
}
