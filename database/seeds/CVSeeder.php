<?php

use Illuminate\Database\Seeder;

use App\User;
use App\Cv;

class CVSeeder extends Seeder
{
    public function run()
    {
        $e1 = User::where('name', '=', 'etudiant')->first()->role;

        $cv = new CV();
        $cv->description = 'CV #1';
        $cv->etudiant()->associate($e1);
        $cv->save();

        $cv = new CV();
        $cv->description = 'CV #2';
        $cv->etudiant()->associate($e1);
        $cv->save();

    }
}
